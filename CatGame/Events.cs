﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatGame
{
    delegate void UI ();

    class Events
    {
        public event UI DeathEvent;

        public void OnDeathEvant()
        {
            DeathEvent();
        }
    }
}
