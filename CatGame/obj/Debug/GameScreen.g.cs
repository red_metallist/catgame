﻿#pragma checksum "C:\Users\Николай\documents\visual studio 2013\Projects\CatGame\CatGame\GameScreen.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "95C42A3FD4CC73899956C1E38C14AE0B"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.34209
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace CatGame {
    
    
    public partial class GameScreen : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Image Room;
        
        internal System.Windows.Controls.ProgressBar HealthProgressValue;
        
        internal System.Windows.Controls.ProgressBar FunProgressValue;
        
        internal System.Windows.Controls.ProgressBar ClearProgressValue;
        
        internal System.Windows.Controls.TextBlock PetName;
        
        internal System.Windows.Controls.TextBlock HealthValue;
        
        internal System.Windows.Controls.TextBlock FunValue;
        
        internal System.Windows.Controls.TextBlock ClearValue;
        
        internal System.Windows.Controls.Image Cat;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/CatGame;component/GameScreen.xaml", System.UriKind.Relative));
            this.Room = ((System.Windows.Controls.Image)(this.FindName("Room")));
            this.HealthProgressValue = ((System.Windows.Controls.ProgressBar)(this.FindName("HealthProgressValue")));
            this.FunProgressValue = ((System.Windows.Controls.ProgressBar)(this.FindName("FunProgressValue")));
            this.ClearProgressValue = ((System.Windows.Controls.ProgressBar)(this.FindName("ClearProgressValue")));
            this.PetName = ((System.Windows.Controls.TextBlock)(this.FindName("PetName")));
            this.HealthValue = ((System.Windows.Controls.TextBlock)(this.FindName("HealthValue")));
            this.FunValue = ((System.Windows.Controls.TextBlock)(this.FindName("FunValue")));
            this.ClearValue = ((System.Windows.Controls.TextBlock)(this.FindName("ClearValue")));
            this.Cat = ((System.Windows.Controls.Image)(this.FindName("Cat")));
        }
    }
}

