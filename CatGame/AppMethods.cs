﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pets;


namespace CatGame
{
    static class AppMethods
    {
        static public string[] TakeMass(Pet UserPet)
        {
            string[] mass = new string[8];
            mass[0] = Convert.ToString(UserPet.Clear);
            mass[1] = Convert.ToString(UserPet.Fun);
            mass[2] = Convert.ToString(UserPet.Health);
            mass[3] = Convert.ToString(UserPet.Hungry);
            mass[4] = Convert.ToString(UserPet.Age);
            mass[5] = Convert.ToString(UserPet.Name);
            mass[6] = Convert.ToString(UserPet.Sex);
            mass[7] = Convert.ToString(UserPet.Type);
            return mass;
        }
        static public Cat LoadCat(string[] status)
        {
            Cat LoadCat = new Cat(status[5], Convert.ToBoolean(status[6]));
            LoadCat.Clear = Convert.ToByte(status[0]);
            LoadCat.Fun = Convert.ToByte(status[1]);
            LoadCat.Health = Convert.ToByte(status[2]);
            LoadCat.Hungry = Convert.ToByte(status[3]);
            LoadCat.Age = Convert.ToByte(status[4]);
            //LoadCat.Type = status[7];
            return LoadCat;
        }
        static public Cat NewStatus(Cat OldCat, int minutes)
        {
            Cat NewCat = new Cat(OldCat.Name, OldCat.Sex);
            OldCat.ChangeClear(minutes);
            OldCat.ChangeFun(minutes);
            OldCat.ChangeHealth(OldCat.Fun, minutes);
            OldCat.ChangeHungry(minutes);
            return OldCat;
        }
        static public Cat GetPet(string Name, bool Sex)
        {
            if (StatusSaver.GetPetStatus() == false)
            {
                return new Cat(Name, Sex);
            }
            else
            {
                return LoadCat(StatusSaver.LoadCatStatus("Status.txt"));
            }
        }
    }
}
