﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pets;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using Pets;

namespace CatGame
{
    class SerializeLayer
    {
        private const string FOLDER_PATH = "bluetooth";
        private const string POST_FILE_NAME = "post";
        private const string GET_FILE_NAME = "get";
        private DataContractSerializer serializer;
        private IsolatedStorageFile isolatedFile;
        private static readonly SerializeLayer instance = new SerializeLayer();
        public static SerializeLayer Instance { get { return instance; } }
        private SerializeLayer()
        {
            serializer = new DataContractSerializer(typeof(Pet));
        }
        public string GetLoadPath()
        {
            return String.Format("{0}/{1}.dat", FOLDER_PATH, GET_FILE_NAME);
        }
        public string GetSavePath()
        {
            return String.Format("{0}/{1}.dat", FOLDER_PATH, POST_FILE_NAME);
        }
        IsolatedStorageFile IsoFile
        {
            get
            {
                if (isolatedFile == null)
                    isolatedFile = System.IO.IsolatedStorage.
                                IsolatedStorageFile.GetUserStoreForApplication();
                return isolatedFile;
            }
        }
        public void SaveData(Pet pet)
        {
            string name = GetSavePath();
            if (!IsoFile.DirectoryExists(name))
            {
                IsoFile.CreateDirectory(name);
            }
            try
            {
                using (var File = IsoFile.CreateFile(name))
                {
                    serializer.WriteObject(File, pet);
                }
            }
            catch (Exception e)
            {
                IsoFile.DeleteFile(name);
            } 
        }
        public Pet LoadData()
        {
            Pet pet=null;
            string name = GetLoadPath();
            if (IsoFile.FileExists(name))
            {


                using (var sourceStream =
                        IsoFile.OpenFile(name, FileMode.Open))
                {
                    pet = (Pet)serializer.ReadObject(sourceStream);
                }
            }
            return pet;
        }
    }
}
