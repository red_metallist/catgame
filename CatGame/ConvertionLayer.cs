﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatGame
{
    class ConvertionLayer
    {
        private static readonly ConvertionLayer instance = new ConvertionLayer();
        public static ConvertionLayer Instance { get { return instance; } }
        private ConvertionLayer()
        {
        }
        public bool ByteArrayToFile(byte[] ByteArray)
        {
            bool isSuccessfull = false;
            string name = SerializeLayer.Instance.GetLoadPath();
            try
            {
                System.IO.FileStream stream = new System.IO.FileStream(name, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                stream.Write(ByteArray, 0, ByteArray.Length);
                stream.Close();
                isSuccessfull = true;
            }
            catch(Exception e)
            {
                isSuccessfull = false;
            }
            return isSuccessfull;
        }
        public byte[] FileToByteArray()
        {
            string name = SerializeLayer.Instance.GetSavePath();
            byte[] bytes = null;
            System.IO.FileStream stream = new System.IO.FileStream(name, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read);
            bytes = new byte[(int)stream.Length];
            stream.Read(bytes, 0, (int)stream.Length);
            stream.Close();
            return bytes;
        }
    }
}
