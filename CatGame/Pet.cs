﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pets
{
   public enum Skin
    { }

   public abstract class Pet
    {

        public Skin skin;
        protected string _name;
        protected byte _health;
        protected byte _fun;
        protected byte _hungry;
        protected bool _sex;
        protected byte _age;
        protected byte _clear;
        protected string _type;
        /*
         * По поводу некоторых параметров входящих в методы
         * Minutes - Количество минут прошедших с прошлого входа в игру
         * TotalMinutes - Количество минут всего проведенных в игре
         */

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public byte Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
            }
        }

        public byte Fun
        {
            get
            {
                return _fun;
            }
            set
            {
                _fun = value;
            }
        }

        public byte Hungry
        {
            get
            {
                return _hungry;
            }
            set
            {
                _hungry = value;
            }
        }

        public bool Sex
        {
            get
            {
                return _sex;
            }
        }
        public byte Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }


        public byte Clear
        {
            get
            {
                return _clear;
            }
            set
            {
                _clear = value;
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }
        }

        public override bool Equals(object obj)
        {
            return this._type == ((Pet)obj).Type;
        }

        //abstract protected string GetVoice();

        virtual public void ChangeFun(int minutes)
        {
            //Процент в полчаса
            if (minutes < 3000)
            {
                if ((_fun - minutes / 30) > 0)
                {
                    _fun = Convert.ToByte(_fun - minutes / 30);
                }
                else
                {
                    _fun = 0;
                }
            }
            else
            {
                _fun = 0;
            }
        }

        virtual public void ChangeHungry(int minutes)
        {
            if (minutes < 6000)
            {
                if ((_hungry - minutes / 60) > 0)
                {
                    _hungry = Convert.ToByte(_hungry - minutes / 60);
                }
                else
                {
                    _hungry = 0;
                }
            }
            else
            {
                _hungry = 0;
            }
        }

        virtual public void ChangeAge(int TotalMinutes)
        {
            _age = Convert.ToByte(_age + TotalMinutes / 10080);
        }

        virtual public void ChangeClear(int minutes)
        {
            if (minutes < 6000)
            {
                if ((_clear - minutes / 60) > 0)
                {
                    _clear = Convert.ToByte(_clear - minutes / 60);
                }
                else
                {
                    _clear = 0;
                }
            }
            else
            {
                _clear = 0;
            }
        }
        virtual public void ChangeHealth(byte _clear, int minutes)
        {
            if (minutes < 6000)
            {
                if (_clear >= 60)
                {
                    if ((_health - minutes / 60) > 0)
                    {
                        _health = Convert.ToByte(_health - minutes / 60);
                    }
                    else
                    {
                        _health = 0;
                    }
                }
                else if (_clear >= 40)
                {
                    if ((_health - minutes / 55) > 0)
                    {
                        _health = Convert.ToByte(_health - minutes / 55);
                    }
                    else
                    {
                        _health = 0;
                    }
                }
                else if (_clear >= 20)
                {
                    if ((_health - minutes / 50) > 0)
                    {
                        _health = Convert.ToByte(_health - minutes / 50);
                    }
                    else
                    {
                        _health = 0;
                    }
                }
                else
                {
                    if ((_health - minutes / 40) > 0)
                    {
                        _health = Convert.ToByte(_health - minutes / 40);
                    }
                    else
                    {
                        _health = 0;
                    }
                }
            }
            else
            {
                _health = 0;
            }
        }

        virtual public void AddHealth()
        {
            if (_health < 100)
            {
                _health = Convert.ToByte(_health + 10);
            }
        }

        virtual public void AddClear()
        {
            if (_clear < 100)
            {
                _clear = Convert.ToByte(_clear + 10);
            }
        }

        virtual public void AddHungry()
        {
            if (_hungry < 100)
            {
                _hungry = Convert.ToByte(_hungry + 10);
            }
        }

        virtual public void AddFun()
        {
            if (_fun < 100)
            {
                _fun = Convert.ToByte(_fun + 10);
            }
        }

        virtual protected bool GetStatus()
        {
            if ((_fun == 0) && (_clear < 40) && (_health < 15) && (_hungry < 20))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
