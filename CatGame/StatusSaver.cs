﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Pets;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;

namespace CatGame
{
    class StatusSaver
    {
        public static async Task SaveStatus(string TestFile, string[] info)
        {
            await Task.Delay(0);
           /* FileStream fs = new FileStream(@"D:\\Status.txt", FileMode.Create);
            
            fs.Close();
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("lol");
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

            // Create a new folder name DataFolder.
            var dataFolder = await local.CreateFolderAsync("DataFolder",
                CreationCollisionOption.OpenIfExists);
            var file = await dataFolder.CreateFileAsync("DataFile.txt",
   CreationCollisionOption.ReplaceExisting);
            byte[] mass = new byte[info.Length];
            for (int a = 0; a != info.Length;a++ )
            {
                mass[a] = Convert.ToByte(info[a]);
            }
                // Write the data from the textbox.
                using (var s = await file.OpenStreamForWriteAsync())
                {
                    s.Write(mass, 0, mass.Length);
                }

            
           // for (int i = 0; i < info.Length; i++)
            //{
              //  sw.WriteLineAsync(info[i]);
            //}
           sw.Close();
            //await Task.Delay(0);
           fs.Close();
            * */
         
            StreamWriter sw = new StreamWriter(TestFile, false);

            for (int i = 0; i < info.Length; i++)
            {
                sw.WriteLine(info[i]);
            }
            sw.Close();
           

        }
        public static string[] LoadCatStatus(string TestFile)
        //where T: Cat
        {
            StreamReader sr = new StreamReader(TestFile);
            string[] status = new string[8];
            for (int i = 0; i < status.Length; i++)
            {
                status[i] = sr.ReadLine();
            }
            sr.Close();
            return status;
        }
        public static void SavePoint(string FileName, int[] Point)
        {
            StreamWriter sw = new StreamWriter(FileName, false);
            for (int i = 0; i < Point.Length; i++)
            {
                sw.WriteLine(Point[i]);
            }
            sw.Close();
        }
        public static int[] LoadPoint(string FileName)
        {
            StreamReader sr = new StreamReader(FileName);
            int[] Point = new int[2];
            for (int i = 0; i < Point.Length; i++)
            {
                Point[i] = Int32.Parse(sr.ReadLine());
            }
            sr.Close();
            return Point;
        }
        public static void SavePetStatus(bool IsAlive)
        {
            StreamWriter sw = new StreamWriter("PetStatus", false);
            sw.WriteLine(IsAlive);
            sw.Close();
        }
        public static bool GetPetStatus()
        {
            StreamReader sr = new StreamReader("PetStatus.txt");
            return Convert.ToBoolean(sr.ReadLine());
        }
    }
}
