﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using CatGame.Resources;
using System.Diagnostics;
using System.Threading;
using System.IO;


namespace CatGame
{
    public partial class GameScreen : PhoneApplicationPage
    {
        public string st;
        public int b = 0;
        public int tab = 0;
        public static string petname;
       // public Cat MyCat = new Cat(GameScreen.petname, true);
        public Cat MyCat = AppMethods.LoadCat(StatusSaver.LoadCatStatus("Status.txt"));
        public GameScreen()
        {
            InitializeComponent();
            //Thread th = new Thread(Animation);
            //th.Start();
            Cat.Source = new BitmapImage(new Uri(@"FunnyCat" + @"/" + "0"+ ".PNG", UriKind.RelativeOrAbsolute));
            if (StatusSaver.GetPetStatus() == true)
            {
                PetName.Text = Convert.ToString(MyCat.Name);
                //GameScreen.MyCat = new Cat1(MainPage.petname, true);
                FunProgressValue.Value = MyCat.Fun;
                FunValue.Text = Convert.ToString(MyCat.Fun);
                ClearProgressValue.Value = MyCat.Clear;
                ClearValue.Text = Convert.ToString(MyCat.Clear);
                HealthProgressValue.Value = MyCat.Health;
                HealthValue.Text = Convert.ToString(MyCat.Health);
            }
            else
            {
                PetName.Text = Convert.ToString(GameScreen.petname);
                //GameScreen.MyCat = new Cat1(MainPage.petname, true);
                MyCat.Fun = 100;
                MyCat.Clear = 100;
                MyCat.Health = 100;
                FunProgressValue.Value = 100;
                FunValue.Text = "100";
                ClearProgressValue.Value = 100;
                ClearValue.Text = "100";
                HealthProgressValue.Value =100;
                HealthValue.Text = "100";
                
            }
         
        }


        public async Task Start()
        {
            await Task.Delay(30);
            Sourse();


        }
        public void Sourse()
        {
            Cat.Source = new BitmapImage(new Uri(@"FunnyCat" + @"/"  + st + ".PNG", UriKind.RelativeOrAbsolute));
        }

        private void StartAnimation()
        {
            //b = 0;
            string str = Convert.ToString(b);
            st = str;
            Start();
            b++;
            if (b == 12)
                b = 0;

            /* while(true)
             {
                 for(int a=0;a!=12;a++)
                 {
                     int b = a;
                     string str = Convert.ToString(b);
                     st = str;
                     Start();
                 }
             }
             * */
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MyCat.AddHealth();
            StartAnimation();
            HealthProgressValue.Value = MyCat.Health;
            HealthValue.Text = Convert.ToString(MyCat.Health);
            StatusSaver.SaveStatus("Status.txt", AppMethods.TakeMass(MyCat));
           
          

        }

        private void Pic_ImageOpened(object sender, RoutedEventArgs e)
        {
            if (tab <= 22)
            {
                StartAnimation();
                tab++;
            }
            else
                tab = 0;

        }

       

      

        public void Main()
        {/*
            //!!!!!!!!!!!!ПОСЛЕ КАЖДОГО ПЕРЕРАСЧЕТА ПАРАМЕТРОВ МИНУТЫ ДОЛЖНЫ ОБНУЛЯТЬСЯ, ЛИБО ПЕРЕРАСЧЕТ ПАРАМЕТРОВ ДОЛЖЕН БЫТЬ ТОЛЬКО ВНАЧАЛЕ И В КОНЦЕ ИГРЫ!!!!
            int minutes = 0;
            Stopwatch sp = new Stopwatch();
            DateTime Start = DateTime.Now;
            //DateTime NowTime = new DateTime();
         //   Console.WriteLine("Введите имя животного");
          //  string name = Console.ReadLine();
            //Cat1 MyCat = new Cat1(name, true); //True - значит мужчина ( а кто тут такой сексист? c: )
            Threads SecondThread = new Threads(MyCat, sp);
            int AppStatus = 0;
            Console.WriteLine("1 - Add fun\n2 - Add health\n3 - AddClear\n4 - Add Hungry\n5 - Save Cat\n6 - Load Cat\n7 - Minutes in Game\n8 - Change Status\n9 - Minutes+10\n10 - Exit");
            Console.WriteLine("-=////////////////=-");
            do
            {
                //MyCat.
                Console.WriteLine("Health - {0}\nClear - {1}\nFun - {2}\nHungry - {3}\nAge - {4}\nName - {5}\nSex - {6}\nType - {7}", MyCat.Health, MyCat.Clear, MyCat.Fun, MyCat.Hungry, MyCat.Age, MyCat.Name, MyCat.Sex, MyCat.Type);
                AppStatus = Int32.Parse(Console.ReadLine());

                sp.Start();
                switch (AppStatus)
                {

                  //  case 1: MyCat.AddFun();
                    //    break;
                  //  case 2: MyCat.AddHealth();
                    //    break;
                  //  case 3: MyCat.AddClear();
                    //    break;
                //    case 4: MyCat.AddHungry(); места нет на форме, поэтому это не юзаем.
                  //      break;
                    case 5: StatusSaver.SaveStatus("Status.txt", AppMethods.TakeMass(MyCat));//AppMethod - Записывает все в массив для удобной обработки, StatusSaver - Сохраняет
                        Console.WriteLine("Кот сохранен");
                        break;
                    case 6: MyCat = AppMethods.LoadCat(StatusSaver.LoadCatStatus("Status.txt"));
                        SecondThread = new Threads(MyCat, sp);
                        Console.WriteLine("Кот загружен");
                        break;
                    case 7: Console.WriteLine("Milliseconds - {0}, Minutes - {1}", sp.ElapsedMilliseconds, minutes);
                        break;
                    case 8: minutes = minutes + Convert.ToInt32(sp.ElapsedMilliseconds / 60000);
                        MyCat = AppMethods.NewStatus(MyCat, minutes);
                        break;
                    case 9: minutes += 10;
                        break;
                    case 10: Console.WriteLine("Bye Bye");
                        break;
                    default: Console.WriteLine("Повторите выбор");
                        break;
                }
                Console.WriteLine("-=////////////////=-");

            }
            while (AppStatus != 10);
            sp.Stop();
           // Console.WriteLine("Milliseconds - {0}, Minutes - {1}", sp.ElapsedMilliseconds, sp.ElapsedMilliseconds / 60000);
            /*
            Console.WriteLine("Сохраняем статус кота");
            StatusSaver.SaveStatus("Status.txt",AppMethods.TakeMass(MyCat));//AppMethod - Записывает все в массив для удобной обработки, StatusSaver - Сохраняет
            Cat LoadCat = AppMethods.LoadCat(StatusSaver.LoadCatStatus("Status.txt"));
            Console.WriteLine("Новый кот загружен со следующими характеристиками");
            Console.WriteLine("Health - {0}\nClear - {1}\nFun - {2}\nHungry - {3}\nAge - {4}\nName - {5}\nSex - {6}\nType - {7}", LoadCat.Health, LoadCat.Clear, LoadCat.Fun, LoadCat.Hungry, LoadCat.Age, LoadCat.Name, LoadCat.Sex, LoadCat.Type);
             */
            
        }
    

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MyCat.AddFun();
            StartAnimation();
            FunProgressValue.Value = MyCat.Fun;
            FunValue.Text = Convert.ToString(MyCat.Fun);
            StatusSaver.SaveStatus("Status.txt", AppMethods.TakeMass(MyCat));
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            MyCat.AddClear();
            StartAnimation();
            ClearProgressValue.Value = MyCat.Clear;
            ClearValue.Text = Convert.ToString(MyCat.Clear);
            StatusSaver.SaveStatus("Status.txt", AppMethods.TakeMass(MyCat));
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            HealthProgressValue.Value = MyCat.Health;
            HealthValue.Text = Convert.ToString(MyCat.Health);
           
            
        }
        private void ProgressBar_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            FunProgressValue.Value = MyCat.Fun;
            FunValue.Text = Convert.ToString(MyCat.Fun);
        }

        private void ProgressBar_ValueChanged_2(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ClearProgressValue.Value = MyCat.Clear;
            ClearValue.Text = Convert.ToString(MyCat.Clear);
            
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            BluetoothLayer.Instance.Search();
            BluetoothLayer.Instance.PostConnection();
        }
       
       
    }
}