﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using CatGame.Resources;
using System.ComponentModel;

namespace CatGame
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Конструктор
        
        public MainPage()
        {
            InitializeComponent();
            if (StatusSaver.GetPetStatus() == true)
            {
              //  NavigationService.Navigate(new Uri("/GameScreen.xaml", UriKind.Relative));
                Name1.IsEnabled=false;
               
            }
            else
            {
                
            }

            // Пример кода для локализации ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            if (Convert.ToString(Name1.Text) == "" && StatusSaver.GetPetStatus() == true)
            {
                GameScreen gs = new GameScreen();
                NavigationService.Navigate(new Uri("/GameScreen.xaml", UriKind.Relative));
            }
            else
            {
                //GameScreen gs = new GameScreen();
                GameScreen.petname = Name1.Text.ToString();
              
                
                NavigationService.Navigate(new Uri("/GameScreen.xaml",UriKind.Relative));
              
                
               
            }

        }

        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Environment.FailFast("Exit");
        }

       

     

       
       
        // Пример кода для сборки локализованной панели ApplicationBar
        //private void BuildLocalizedApplicationBar()
       

        //    // Установка в качестве ApplicationBar страницы нового экземпляра ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Создание новой кнопки и установка текстового значения равным локализованной строке из AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Создание нового пункта меню с локализованной строкой из AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}