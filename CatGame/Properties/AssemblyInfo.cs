﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// Управление общими сведениями о сборке осуществляется с помощью следующего 
// набора атрибутов. Измените значения этих атрибутов для изменения
// сведений о сборке.
[assembly: AssemblyTitle("CatGame")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("sibiryak-soft")]
[assembly: AssemblyProduct("CatGame")]
[assembly: AssemblyCopyright("Copyright © sibiryak-soft 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Если для ComVisible установить значение false, типы в этой сборке не будут поддерживаться 
// COM-компонентами.  При необходимости доступа к какому-либо типу в этой сборке 
// из модели COM задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если данный проект видим для COM
[assembly: Guid("f06f72a2-66dd-4be7-a459-43b5736b1f9b")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// с помощью символа '*', как показано ниже:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("ru-RU")]
