﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pets;

namespace CatGame
{
   public class Cat: Pet
    {
        public Cat(string name, bool sex)
        {
            base._age = 0;
            base._clear = 50;
            base._fun = 50;
            base._health = 50;
            base._hungry = 50;
            base._name = name;
            base._sex = sex;//1 - man, 0 - woman
            base._type = "Cat";
        }
        public void Come()
        {
            this._age = ((Cat)SerializeLayer.Instance.LoadData()).Age;
            this._clear = ((Cat)SerializeLayer.Instance.LoadData()).Clear;
            this._fun = ((Cat)SerializeLayer.Instance.LoadData()).Fun;
            this._health = ((Cat)SerializeLayer.Instance.LoadData()).Health;
            this._hungry = ((Cat)SerializeLayer.Instance.LoadData()).Hungry;
            this._name = ((Cat)SerializeLayer.Instance.LoadData()).Name;
            this._sex = ((Cat)SerializeLayer.Instance.LoadData()).Sex;
            this._type = ((Cat)SerializeLayer.Instance.LoadData()).Type;
        }


    }
}
