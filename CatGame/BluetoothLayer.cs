﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Proximity;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Runtime.InteropServices.WindowsRuntime;
using Pets;
namespace CatGame
{
    class BluetoothLayer
    {
        private static readonly BluetoothLayer instance = new BluetoothLayer();
        public static BluetoothLayer Instance { get { return instance; } }
        private bool isFind = false;
        private IReadOnlyList<PeerInformation> peers;
        private BluetoothLayer()
        {
        }
        public async void Search()
        {
            PeerFinder.Start();
            peers = await PeerFinder.FindAllPeersAsync();
            if (peers.Count != 0) { isFind = true; }
        }
        public async void GetConnection()
        {
            if (isFind)
            {
                PeerInformation currentPeer = peers[0];
                var streamSocket = await PeerFinder.ConnectAsync(currentPeer);
                IBuffer buffer = new byte[1024].AsBuffer();
                await streamSocket.InputStream.ReadAsync(buffer,buffer.Capacity,Windows.Storage.Streams.InputStreamOptions.Partial);
                ConvertionLayer.Instance.ByteArrayToFile(buffer.ToArray());
                streamSocket.Dispose();
            }
        }
         public async void PostConnection()
        {
            if (isFind)
            {
                PeerInformation currentPeer = peers[0];
                var streamSocket = await PeerFinder.ConnectAsync(currentPeer);
                IBuffer buffer = ConvertionLayer.Instance.FileToByteArray().AsBuffer();
                await streamSocket.OutputStream.WriteAsync(buffer);
                streamSocket.Dispose();
            }
        }
    }
}
