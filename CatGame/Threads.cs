﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Pets;
using System.Diagnostics;

namespace CatGame
{
    class Threads
    {
        Thread StatusCounter;
        Stopwatch _Time;
        
        public Threads(Pet UserPet,Stopwatch Time)
        {
            object[] parametrs = new object[2];
            StatusCounter = new Thread(this.Counter);
            StatusCounter.Start(UserPet);
            _Time = Time;
        }
        void Counter (object UserPet)
        {
            Pet UP = (Pet)UserPet;
            start:
            System.Threading.Thread.Sleep(600);
            UP.ChangeClear((int)_Time.ElapsedMilliseconds /5);
            //UP.ChangeFun((int)_Time.ElapsedMilliseconds /5);
            UP.ChangeHealth(UP.Clear, (int)_Time.ElapsedMilliseconds /10);
            UP.ChangeHungry((int)_Time.ElapsedMilliseconds /5);
            goto start;
        }
    }
}
